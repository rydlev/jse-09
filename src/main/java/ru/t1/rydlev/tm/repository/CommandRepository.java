package ru.t1.rydlev.tm.repository;

import ru.t1.rydlev.tm.api.repository.ICommandRepository;
import ru.t1.rydlev.tm.constant.ArgumentConstant;
import ru.t1.rydlev.tm.constant.TerminalConstant;
import ru.t1.rydlev.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(
            TerminalConstant.HELP, ArgumentConstant.HELP,
            "Show application commands."
    );

    private static final Command VERSION = new Command(
            TerminalConstant.VERSION, ArgumentConstant.VERSION,
            "Show application version."
    );

    private static final Command INFO = new Command(
            TerminalConstant.INFO, ArgumentConstant.INFO,
            "Show developer info."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConstant.ARGUMENTS, ArgumentConstant.ARGUMENTS,
            "Show developer arguments."
    );

    private static final Command COMMANDS = new Command(
            TerminalConstant.COMMANDS, ArgumentConstant.COMMANDS,
            "Show developer commands."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConstant.PROJECT_LIST, null,
            "Show project list."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConstant.PROJECT_CREATE, null,
            "Create new project."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConstant.PROJECT_CLEAR, null,
            "Remove all projects."
    );

    private static final Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalConstant.PROJECT_SHOW_BY_INDEX, null,
            "Display project by index."
    );

    private static final Command PROJECT_SHOW_BY_ID = new Command(
            TerminalConstant.PROJECT_SHOW_BY_ID, null,
            "Display project by id."
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConstant.PROJECT_UPDATE_BY_INDEX, null,
            "Update project by index."
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConstant.PROJECT_UPDATE_BY_ID, null,
            "Update project by id."
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConstant.PROJECT_REMOVE_BY_INDEX, null,
            "Remove project by index."
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConstant.PROJECT_REMOVE_BY_ID, null,
            "Remove project by id."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConstant.TASK_LIST, null,
            "Show task list."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConstant.TASK_CREATE, null,
            "Create new task."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConstant.TASK_CLEAR, null,
            "Remove all tasks."
    );

    private static final Command TASK_SHOW_BY_INDEX = new Command(
            TerminalConstant.TASK_SHOW_BY_INDEX, null,
            "Display task by index."
    );

    private static final Command TASK_SHOW_BY_ID = new Command(
            TerminalConstant.TASK_SHOW_BY_ID, null,
            "Display task by id."
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConstant.TASK_UPDATE_BY_INDEX, null,
            "Update task by index."
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConstant.TASK_UPDATE_BY_ID, null,
            "Update task by id."
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConstant.TASK_REMOVE_BY_INDEX, null,
            "Remove task by index."
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConstant.TASK_REMOVE_BY_ID, null,
            "Remove task by id."
    );

    private static final Command TASK_BIND_TO_PROJECT = new Command(
            TerminalConstant.TASK_BIND_TO_PROJECT, null,
            "Bind task to project."
    );

    private static final Command TASK_UNBIND_FROM_PROJECT = new Command(
            TerminalConstant.TASK_UNBIND_FROM_PROJECT, null,
            "Unbind task from project."
    );

    private static final Command TASK_SHOW_BY_PROJECT_ID = new Command(
            TerminalConstant.TASK_SHOW_BY_PROJECT_ID, null,
            "Show task list by project id."
    );

    private static final Command EXIT = new Command(
            TerminalConstant.EXIT, null,
            "Close application."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, VERSION, INFO, ARGUMENTS, COMMANDS,

            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            PROJECT_SHOW_BY_INDEX, PROJECT_SHOW_BY_ID,
            PROJECT_UPDATE_BY_INDEX, PROJECT_UPDATE_BY_ID,
            PROJECT_REMOVE_BY_INDEX, PROJECT_REMOVE_BY_ID,

            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            TASK_SHOW_BY_INDEX, TASK_SHOW_BY_ID,
            TASK_UPDATE_BY_INDEX, TASK_UPDATE_BY_ID,
            TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_ID,
            TASK_BIND_TO_PROJECT, TASK_UNBIND_FROM_PROJECT,
            TASK_SHOW_BY_PROJECT_ID,

            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
