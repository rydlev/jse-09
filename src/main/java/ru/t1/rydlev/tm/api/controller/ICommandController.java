package ru.t1.rydlev.tm.api.controller;

public interface ICommandController {

    void showWelcome();

    void showHelp();

    void showCommands();

    void showArguments();

    void showVersion();

    void showDeveloperInfo();

    void showCommandError();

    void showArgumentError();

}
