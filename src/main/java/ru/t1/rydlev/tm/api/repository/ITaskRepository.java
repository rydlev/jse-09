package ru.t1.rydlev.tm.api.repository;

import ru.t1.rydlev.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAllByProjectId(String projectId);

    Task add(Task task);

    void deleteAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}
