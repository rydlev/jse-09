package ru.t1.rydlev.tm.api.repository;

import ru.t1.rydlev.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
