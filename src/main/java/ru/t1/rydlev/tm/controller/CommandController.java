package ru.t1.rydlev.tm.controller;

import ru.t1.rydlev.tm.api.controller.ICommandController;
import ru.t1.rydlev.tm.api.service.ICommandService;
import ru.t1.rydlev.tm.model.Command;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showWelcome() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        for (final Command command : commandService.getTerminalCommands()) {
            System.out.println(command);
        }
    }

    @Override
    public void showCommands() {
        System.out.println("[COMMANDS]");
        for (final Command command : commandService.getTerminalCommands()) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void showArguments() {
        System.out.println("[ARGUMENTS]");
        for (final Command command : commandService.getTerminalCommands()) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.4.0");
    }

    @Override
    public void showDeveloperInfo() {
        System.out.println("[DEVELOPER]");
        System.out.println("NAME: Oleg Rydlev");
        System.out.println("E-MAIL: work.akrr@gmail.com");
    }

    @Override
    public void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This command is not supported...");
    }

    @Override
    public void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported...");
        System.exit(1);
    }

}
